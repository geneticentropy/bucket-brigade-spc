# Installation

From the SPC repo directory, run:

```
# Uninstall any previously installed bucket_brigade
./spc uninstall bucket_brigade

# Install
./spc install https://bitbucket.org/geneticentropy/bucket-brigade-spc/get/master.zip

# Create symlink to bucket-brigade binary
ln -s $BUCKET_BRIGADE_REPO/bucket-brigade ./src/spc_apps/bucket_brigade/bucket_brigade
```
