(function() {
    initPopover();
    initBooleanFields();
    initJobsDialog();

    function initPopover() {
        $('[data-toggle="popover"]').popover({
            container: 'body',
            html: true,
        });
    }

    function initBooleanFields() {
        var form = document.querySelector('.input-form');
        var fields = form.querySelectorAll('.boolean-form-field');

        for (var i = 0; i < fields.length; ++i) {
            initField(form, fields[i]);
        }

        function initField(form, field) {
            var checkboxInput = field.querySelector('.boolean-form-field__checkbox');
            var hiddenInput = field.querySelector('.boolean-form-field__hidden');

            form.addEventListener('submit', onSubmit);

            function onSubmit() {
                hiddenInput.value = checkboxInput.checked ? 'True' : 'False';
            }
        }
    }

    function initJobsDialog() {
        var button = document.querySelector('.jobs-dialog__browse-button');
        var dialog = document.querySelector('.jobs-dialog');
        var dialogContent = dialog.querySelector('.jobs-dialog__content');
        var dialogRows = dialogContent.querySelectorAll('tbody > tr');
        var caseIdElement = document.querySelector('.jobs-dialog__browse-value');
        var input = document.querySelector('form.input-form input[name="input_directory"]');

        button.addEventListener('click', open);

        new EventBubbleIgnore({
            targetElement: dialog,
            ignoreElements: [dialogContent],
            eventName: 'click',
            callback: close,
        });

        for (var i = 0; i < dialogRows.length; ++i) {
            initRow(dialogRows[i]);
        }

        function initRow(row) {
            var directory = row.getAttribute('data-directory');
            var caseId = row.getAttribute('data-case-id');

            row.addEventListener('click', onClick);

            function onClick() {
                caseIdElement.textContent = 'Case ID: ' + caseId;
                input.value = directory;
                close();
            }
        }

        function open() {
            document.body.classList.add('jobs-dialog--open');
        }

        function close() {
            document.body.classList.remove('jobs-dialog--open');
        }
    }
}());
