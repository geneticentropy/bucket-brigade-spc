function EventBubbleIgnore(options) {
    var listen = function(element) {
        element.addEventListener(this.eventName, this.handleIgnore);
    }.bind(this);

    this.bindThis();

    this.targetElement = options.targetElement;
    this.ignoreElements = options.ignoreElements;
    this.eventName = options.eventName;
    this.callback = options.callback;

    this.ignoreBubble = false;

    for (var i = 0; i < this.ignoreElements.length; ++i) {
        listen(this.ignoreElements[i]);
    }

    this.targetElement.addEventListener(this.eventName, this.handleTarget);
}

EventBubbleIgnore.prototype.bindThis = function() {
    this.handleTarget = this.handleTarget.bind(this);
    this.handleIgnore = this.handleIgnore.bind(this);
};

EventBubbleIgnore.prototype.handleTarget = function(e) {
    if (!this.ignoreBubble) {
        this.callback(e);
    }

    this.ignoreBubble = false;
};

EventBubbleIgnore.prototype.handleIgnore = function(e) {
    this.ignoreBubble = true;
};
