<%
    from spc_apps.bucket_brigade.settings import get_jobs

	jobs = get_jobs()
%>

<!DOCTYPE html>
<html>
<head>
    <title>{{title}}</title>

    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <link rel="stylesheet" href="/static/css/bootstrap.min.css" />
    <link rel="stylesheet" href="/static/css/bootstrap-tagsinput.css" />
    <link rel="stylesheet" href="/static/css/style-metro.css" />

    <link rel="stylesheet" href="/static/apps/bucket_brigade/bucket_brigade.css" />
</head>
<body>
    <div class="container-fluid">
        %include('navbar')
        %include('apps/alert')
        <div id="memory" class="alert-info hidden-xs"></div>
        <div id="danger" class="alert-danger"></div>
        <div id="warning" class="alert-warning"></div>
    </div>


    <form class="input-form" name="mendel_input" method="post" action="/confirm">
        <input type="hidden" name="app" value="{{app}}">
        <input type="hidden" name="output_directory" value="./">

        <a class="user-manual-link" href="/static/apps/mendel/help.html" target="_blank">User Manual</a>

        <div class="page-width">
            <div class="page-width__inner">
                <div class="form-sections">
                    <div class="form-section">
                        <div class="form-section__title">Basic</div>
                        <div class="form-section__fields">
                            <div class="form-row">
                                <label for="num_buckets" class="form-row__label">1. Number of buckets:</label>

                                <div class="form-row__widget">
                                    <div class="number-form-field">
                                        <input class="form-control" id="num_buckets" type="number" name="num_buckets" value="100" min="0" max="100000" step="1" />
                                    </div>
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-row__label">2. Job:</div>
                                <div class="form-row__widget jobs-dialog__browse-widget">
                                    <div class="jobs-dialog__browse-button">Browse</div><div class="jobs-dialog__browse-value"></div>
                                    <input type="hidden" name="input_directory" />
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-section">
                        <div class="form-section__title">Job</div>
                        <div class="form-section__fields">
                            <div class="form-row">
                                <div class="form-row__label">1. Tags:</div>
                                <div class="form-row__widget">
                                    <input class="form-control tags-input" type="text" id="desc" name="desc" data-role="tagsinput" placeholder="Enter tag…" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <button class="form-submit-button btn btn-success" type="submit">Continue</button>
            </div>
        </div>
    </form>

    <div class="jobs-dialog">
        <div class="jobs-dialog__content">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Case ID</th>
                        <th>App</th>
                        <th>Time Submitted</th>
                    </tr>
                </thead>
                <tbody>
                    % for job in jobs:
                        <tr data-case-id="{{job['cid']}}" data-directory="{{job['data_dir']}}">
                            <td>{{job['cid']}}</td>
                            <td>{{job['app']}}</td>
                            <td>{{job['time_submitted']}}</td>
                        </tr>
                    % end
                </tbody>
            </table>
        </div>
    </div>


    <script src="/static/jquery-2.1.4.min.js"></script>
    <script src="/static/js/bootstrap.min.js"></script>
    <script src="/static/js/jquery.highlight.js"></script>
    <script src="/static/js/jquery.validate.min.js"></script>
    <script src="/static/js/bootstrap-tagsinput.min.js"></script>
    <script src="/static/js/bootstrap-notify.min.js"></script>

    <script src="/static/apps/bucket_brigade/event_bubble_ignore.js"></script>
    <script src="/static/apps/bucket_brigade/bucket_brigade.js"></script>
</body>
</html>
