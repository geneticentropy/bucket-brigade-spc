from __future__ import unicode_literals

import os
import re

from spc.main import authorized
from spc.model import db, jobs, users


def get_jobs():
    user = authorized()
    uid = users(user=user).id
    job_instances = db(jobs.uid==uid).select(orderby=~jobs.id)[:1000]
    job_objs = []

    for job_instance in job_instances:
        data_dir = os.path.abspath('./user_data/{user}/{app}/{cid}'.format(
            user=job_instance.uid.user,
            app=job_instance.app,
            cid=job_instance.cid
        ))

        if dir_has_alleles_file(data_dir):
            job_objs.append({
                'app': job_instance.app,
                'cid': job_instance.cid,
                'time_submitted': job_instance.time_submit,
                'data_dir': data_dir,
            })

    return job_objs


def dir_has_alleles_file(directory):
    if not os.path.isdir(directory):
        return False

    file_names = os.listdir(directory)

    for file_name in file_names:
        file_path = os.path.join(directory, file_name)

        if os.path.isfile(file_path) and re.match('^alleles(\\.\\d+)?\\.json$', file_name):
            return True

    return False
